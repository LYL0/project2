package spellingbee.client;
import javafx.application.*;
import javafx.event.*;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
public class GameTabLettersButton implements EventHandler<ActionEvent>{

	private TextField userInput;
	private String letter;
	/*
	 * constructor
	 */
	public GameTabLettersButton(TextField u, String l) {
		this.userInput=u;
		this.letter=l;
	}
	@Override
	public void handle(ActionEvent action) {
		userInput.appendText(letter);
		
	}

}
