package spellingbee.client;
import spellingbee.server.*;
import java.io.IOException;
import java.nio.file.*;
import java.util.*;
/*
 * Front end of the project, as I am working by myself
 * a simplified version of the regular game
 * Hardcode a choice of 7 letters alone with a center letter(at position 3).
 */
public class SimpleSpellingBeeGame implements ISpellingBeeGame{
	private String letters;
	private char center;
	private int currentScore;
	private ArrayList<String>playerFoundWords;
	private HashSet<String>collectionValidWords;
	private String[] vWords= {"puttin","putins","utpin","tin",
			"nintin", "clad"};
	private int[] points;
	/*
	 * constructor
	 */
	public SimpleSpellingBeeGame() {
		//Choose a 7 letter word, hard coded.
		this.letters="puttins";
		this.center= this.letters.charAt(3);
		//adding this word to the lists of valid words
		addValidWord(vWords);
	}
	/*
	 * this method updates the score for the user, returning the points 
	 * when an attempt is valid.
	 * if they use all 7 letters(panagram) then the user get 7 more points as bonus.
	 */
	public int getPointForWord(String attempt) {
		//this is the length of the attempt, we would calculate scores base on it.
		int length=attempt.length();
		if(attemptValidate(attempt)== false) {
			//invalid word!! No points.
			return 0;
		}
		else if(length > 4) {
			//record score based on the word's length.
			if(length==7) {
				//Perfect! You get the 7 bonus marks!
				this.currentScore= this.currentScore +length+7;
				return length+7;
			}
			this.currentScore= this.currentScore +length;
		}
		this.currentScore = this.currentScore+1;
		return 1;
	}
	/*
	 * this method would return a message to the user, 
	 * stating their results.
	 */
	public String getMessage(String attempt) {
		playerFoundWords= new ArrayList<String>();
		String message="";
		if (attemptValidate(attempt)==true) {
			message="You did it!";
			return message;
		}
		if(attempt.length()<4) {
			message="4 letters minimum!";
			return message;
		}
		
		if(playerFoundWords.contains(attempt)) {
			message="You put the same word twice?!";
			return message;
		}
		if(attempt.indexOf(center)<0) {
			message="Where is the center letter?";
			return message;
		}

		return "The word is invalid!";
	}
	@Override
	public String getAllLetters() {
		return this.letters;
	}
	@Override
	public char getCenterLetter() {
		return this.center;
	}
	@Override
	public int getScore() {
		return this.currentScore;
	}
	/*
	 * hard code 5 ranking values in increasing order
	 */
	@Override
	public int[] getBrackets() {
		this.points= new int[] {20,35,50,70,90};
		return this.points;
	}
	public void addValidWord(String[] v) {
		collectionValidWords = new HashSet<String>();
		for(String s: v) {
			collectionValidWords.add(s);
			
		}
		
	}
	/*
	 * is the word length greater then 4?
	 * return true if the word has been guess, false otherwise.
	 */
	public boolean attemptValidate(String user) {
		playerFoundWords= new ArrayList<String>();
		// the attempt cannot be less then 4 letters or lack the center letter.
		if(user.length()<4 || user.indexOf(center) <0) {
			return false;
		}
		//verify if the letters from the attempt is found in the 'letter' variable or not.
		//search through the words with a regular for loop.
		for(int i=0; i<user.length();i++) {
			if(letters.indexOf(user.charAt(i))<0){
				//if it does not contain, return false.
				return false;
			}
		}
		if(collectionValidWords.contains(user)) {
			//already had the word?
			if(playerFoundWords.contains(user)) {
				return false;
			}
			//Add the word to the collection for record reasons.
			this.playerFoundWords.add(user);
			return true;
		}
		return false;
	}

	
}
