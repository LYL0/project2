package spellingbee.client;
import spellingbee.network.*;
import javafx.application.*;
import javafx.event.*;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.text.*; 
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.css.*;
public class GameTab extends Tab{
	private Client client;
	private TextField sumPoints;
	private String center;
	private String serverMessage;
	private String serverLetters;
	private String[]letters;
	private String[]message;
	//java fx
	private HBox buttonCollection;
	private Button l1;
	private Button l2;
	private Button l3;
	private Button centerL;
	private Button l4;
	private Button l5;
	private Button l6;
	private Button l7;
	private Button delete;
	private Button clear;
	private Button send;
	private TextField input;
	private TextField messageArea;
	private VBox general;
	private HBox resultsArea;
	private HBox changeButtons;
	private HBox inputArea;
	/*
	 * constructor
	 * take a Client object input
	 */
	private GameTab(Client user) {
		super("Game");
		this.client=user;
		this.setClosable(false);
		this.center=this.client.sendAndWaitMessage("getCenterLetter");
		this.serverLetters=this.client.sendAndWaitMessage("getAllLetters");
		this.letters=parsingServer(serverLetters);
		this.setContent(create());
	}
	/*
	 * this add all the nodes to their containers.
	 */
	public void addingNodes() {
		this.resultsArea.getChildren().addAll(sumPoints,messageArea);
		//add the "controller" buttons
		this.changeButtons.getChildren().addAll(delete, clear,send);
		this.inputArea.getChildren().addAll(input);
		//Add all the buttons to the collection
		this.buttonCollection.getChildren().addAll(l1,l2,l3,l4,l5,l6,l7);
		//Add everything to the big box
		this.general.getChildren().addAll(resultsArea,inputArea,changeButtons,buttonCollection);
	}
	/*
	 * this give the buttons a value
	 */
	public void addLettersToButton() {
		this.buttonCollection= new HBox();
		this.l1 = new Button(letters[0]);
		this.l2 = new Button (letters[1]);
		this.l3 = new Button(letters[2]);
		//this is the center letter
		this.centerL = new Button(letters[3]);
		this.l5 = new Button(letters[4]);
		this.l6 = new Button(letters[5]);
		this.l7 = new Button(letters[6]);
		
	}
	/*
	 * this add the events to buttons.
	 */
	public void addLetterButtonListener() {
		this.l1.setOnAction(new GameTabLettersButton(input,letters[0]));
		this.l2.setOnAction(new GameTabLettersButton(input,letters[1]));
		this.l3.setOnAction(new GameTabLettersButton(input,letters[2]));
		this.centerL.setOnAction(new GameTabLettersButton(input,letters[3]));
		this.l5.setOnAction(new GameTabLettersButton(input,letters[4]));
		this.l6.setOnAction(new GameTabLettersButton(input,letters[5]));
		this.l7.setOnAction(new GameTabLettersButton(input,letters[6]));
	}
	public String[] parsingServer(String serverLetters2) {
		// TODO Auto-generated method stub
		return null;
	}
	public Node create() {
		this.general= new VBox();
		addLettersToButton();
		this.input=new TextField();
		this.inputArea = new HBox();
		this.delete = new Button("Delete");
		this.clear = new Button ("Clear");
		this.send = new Button ("Submit");
		this.resultsArea= new HBox();
		this.messageArea= new TextField("");
		this.sumPoints= new TextField("0");
		setStyles();
		addLetterButtonListener();
		addSubmitListener();
		addClearListener();
		addDelListener();
		addingNodes();
		
		return general;
	}
	private void addDelListener() {
		//delete the last letter in the input.
		this.delete.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent arg0) {
				if(input.getText().length()<=1) {
					input.clear();
				}
				else {
					input.setText(input.getText().substring(0,input.getText().length()-1));
				}
				
			}
			
		});
		
	}
	public void addClearListener() {
		this.clear.setOnAction(new EventHandler<ActionEvent>() {
			//clear anything in the input.
			@Override
			public void handle(ActionEvent action) {
				input.clear();
				
			}
			
		});
		
	}
	public void addSubmitListener() {
		this.send.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent action) {
				if( input.getText().equals("")||input.getText()==null) {
					addInvlaidSub();
				}
				else {
					serverMessage = client.sendAndWaitMessage("getMessage"+input.getText());
					message=parseServerResponse(serverMessage);
					messageArea.setText(message[0]);
					sumPoints.setText(message[1]);
					input.clear();
					
				}
			}
	});
	}


	/*
	 * this shows the user had inputed an invalid value.
	 */
	 public void addInvlaidSub() {
		this.input.clear();
		this.messageArea.setText("Invalid!");
		this.sumPoints.setText("0");
		
		
	}
	 /*
	  * styling for Game Tab
	  */
	public void setStyles() {
		this.general.setStyle("-fx-alignment:center");
		this.buttonCollection.setPrefSize(240,60);
		this.inputArea.setPrefSize(400, 50);
		this.changeButtons.setPrefSize(400, 70);
		this.resultsArea.setPrefSize(400, 50);
		this.sumPoints.setEditable(false);
		this.messageArea.setEditable(false);
		this.l1.setPrefSize(20, 20);
		this.l2.setPrefSize(20, 20);
		this.l3.setPrefSize(20, 20);
		this.centerL.setPrefSize(20, 20);
		this.l5.setPrefSize(20, 20);
		this.l6.setPrefSize(20, 20);
		this.l7.setPrefSize(20, 20);
		this.input.setPrefSize(200, 40);
		this.clear.setPrefSize(100, 30);
		this.delete.setPrefSize(100, 30);
		this.send.setPrefSize(100, 30);
		this.messageArea.setPrefSize(250, 40);
	}
}
